from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.conf import settings
import json
import requests

from .models import Geolocation, Address, AddressGeo


class LocationAdresses(APIView):

    def get_response(self, request):
        api_url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address'
        token = 'Token ' + getattr(settings, 'TOKEN', {})
        headers = {
            'Authorization': token,
            'Content-Type': 'application/json'
        }
        request.data['count'] = 20
        json_data = json.dumps(request.data)
        try:
            response = requests.post(api_url, data=json_data, headers=headers)
        except Exception as error:
            raise Exception(error)
        return response

    def get_addresses(self, response):
        suggestions = response.json().get('suggestions')
        addresses = []
        for address in suggestions:
            addresses.append(address.get('value'))
        return addresses

    def post(self, request):
        lat = request.data['lat']
        lon = request.data['lon']
        radius = request.data.get('radius_meters', 100)
        lang = request.data.get('language', 'ru')
        count = request.data.get('count', 10)
        if radius > 1000:
            radius = 1000
        geo_cached = Geolocation.objects.filter(
            lat=lat, lon=lon, radius=radius, lang=lang
        ).first()
        if geo_cached:
            addresses = geo_cached.addresses.all()[:count].values_list(
                'address', flat=True
            )
            return Response({'Данные из БД': addresses})
        try:
            response = self.get_response(request)
        except Exception as error:
            error_msg = f'Сбой при обращении к API Dadata, ошибка: {error}'
            return Response({'message': error_msg})
        if response.status_code != status.HTTP_200_OK:
            return Response(response.json(), status=response.status_code)
        geo_created = Geolocation.objects.create(
            lat=lat, lon=lon, radius=radius, lang=lang
        )
        addresses_list = self.get_addresses(response)
        objs = []
        for address in addresses_list:
            objs.append(Address(address=address))
        Address.objects.bulk_create(objs, ignore_conflicts=True)
        addr_objs = Address.objects.filter(address__in=addresses_list)
        objs = []
        for obj in addr_objs:
            objs.append(AddressGeo(address=obj, geo=geo_created))
        AddressGeo.objects.bulk_create(objs)
        return Response({'Данные из внешнего API': addresses_list[:count]})
