from django.db import models
from django.db.models import UniqueConstraint


class Geolocation(models.Model):
    lat = models.DecimalField('Широта', max_digits=9, decimal_places=6)
    lon = models.DecimalField('Долгота', max_digits=9, decimal_places=6)
    radius = models.FloatField('Радиус', default=100)
    lang = models.CharField('Язык результата', max_length=2, default='ru')

    class Meta:
        verbose_name = 'Геолокация'
        verbose_name_plural = 'Координаты'
        constraints = [
            UniqueConstraint(
                fields=['lat', 'lon', 'radius', 'lang'], name='u_params'
            ),
        ]

    def __str__(self):
        return f'{self.lat} {self.lon} {self.radius}'


class Address(models.Model):
    address = models.CharField('Адрес', max_length=255, unique=True)
    points = models.ManyToManyField(
        Geolocation,
        through='AddressGeo',
        related_name='addresses')

    class Meta:
        verbose_name = 'Адрес'
        verbose_name_plural = 'Адреса'

    def __str__(self):
        return self.address


class AddressGeo(models.Model):
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    geo = models.ForeignKey(Geolocation, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Кэш ответа'
        verbose_name_plural = 'Ответы'

    def __str__(self):
        return f'{self.address} {self.geo}'
