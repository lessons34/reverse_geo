from django.test import TestCase

from ..models import Geolocation, Address, AddressGeo


class PostModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.address = Address.objects.create(address='test_address')
        cls.geo = Geolocation.objects.create(
            lat=55.878,
            lon=37.653,
            radius=70,
        )
        cls.relation = AddressGeo.objects.create(
            geo=cls.geo,
            address=cls.address
        )

    def test_models_have_correct_object_names(self):
        """Проверяем, что у моделей корректно работает __str__."""
        geo = self.geo
        address = self.address
        relation = self.relation
        expected_geo_name = f'{geo.lat} {geo.lon} {geo.radius}'
        expected_address_name = address.address
        expected_relation_name = f'{relation.address} {relation.geo}'
        self.assertEqual(
            expected_geo_name,
            str(geo),
            'Строковое представление модели "Geolocation" должно равняться конкатенации полей "lat", "lon", "radius".'
        )
        self.assertEqual(
            expected_address_name,
            str(address),
            'Строковое представление модели "Address" должно равняться имени поля "address".'
        )
        self.assertEqual(
            expected_relation_name,
            str(relation),
            'Строковое представление модели "AddressGeo" должно равняться конкатенации полей "address", "geo".'
        )
