from rest_framework import serializers

from .models import User


class RegisterUser(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(min_length=8)

    def validate_username(self, value):
        if User.objects.filter(username=value).exists():
            raise serializers.ValidationError('Это имя пользователя занято.')
        return value


class LoginUser(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(min_length=8)

    def validate_username(self, value):
        if not User.objects.filter(username=value).exists():
            raise serializers.ValidationError('Неверный логин')
        return value

    def validate(self, attrs):
        user = User.objects.filter(username=attrs['username']).first()
        if not user.check_password(attrs['password']):
            raise serializers.ValidationError(
                {'password': 'Неверный пароль'}
            )
        return attrs
