from django.test import TestCase

from ..models import User


class PostModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user(
            username='test_user',
            password='test_password'
        )

    def test_models_have_correct_object_names(self):
        """Проверяем, что у моделей корректно работает __str__."""
        user = self.user
        expected_user_str = user.username

        self.assertEqual(
            expected_user_str,
            str(user),
            'Строковое представление модели "User" должно равняться имени поля "username".'
        )
