from django.contrib import admin

from .models import Geolocation, Address, AddressGeo


class GeolocationAdmin(admin.ModelAdmin):
    list_display = ('lat', 'lon', 'radius', 'lang')


class AddressAdmin(admin.ModelAdmin):
    list_display = ('address',)


class AddressGeoAdmin(admin.ModelAdmin):
    list_display = ('address', 'geo')
    list_filter = ('geo',)


admin.site.register(Geolocation, GeolocationAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(AddressGeo, AddressGeoAdmin)

