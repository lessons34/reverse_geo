from rest_framework.authtoken.models import Token
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status

from .models import User
from .serializers import RegisterUser, LoginUser


class RegisterUser(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterUser
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.create_user(
            username=serializer.validated_data['username'],
            password=serializer.validated_data['password']
        )
        token = Token.objects.create(user=user)
        return Response({'token': token.key}, status=status.HTTP_201_CREATED)


class LoginUser(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = LoginUser
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = Token.objects.get(
            user__username=serializer.validated_data['username']
        )
        return Response({'token': token.key}, status=status.HTTP_200_OK)
