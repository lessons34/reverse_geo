from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

from ..models import User


class RegisterUserTest(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_not_get_allowed(self):
        response = self.client.get(reverse('users:user_register'))
        self.assertEqual(
            response.status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_success_registr(self):
        data = {'username': 'test_user', 'password': 'testpass12345'}
        response = self.client.post(
            path=reverse('users:user_register'),
            data=data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user = User.objects.filter(username=data['username']).first()
        self.assertTrue(user)
        self.assertTrue(user.check_password(data['password']))
