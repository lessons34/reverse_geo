from django.urls import path

from .views import LocationAdresses

app_name = 'core'

urlpatterns = [
    path('', LocationAdresses.as_view(), name='addresses'),
]
